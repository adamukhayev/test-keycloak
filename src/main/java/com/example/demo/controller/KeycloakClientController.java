package com.example.demo.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.keycloak.admin.client.CreatedResponseUtil;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.ClientsResource;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.core.Response;
import java.util.Arrays;

@RestController
@RequestMapping("/api/clients")
@RequiredArgsConstructor
public class KeycloakClientController {

    private final Keycloak keycloak;

    @Operation(summary = "Create client",
            description = "void",
            tags = {"SWAGGER_TAG"})
    @PostMapping("/create/client")
    public void createClient(@RequestBody ClientRepresentation clientRepresentation) {
        RealmResource realmResource = keycloak.realm("todoapp-realm");
        ClientsResource clientsResource = realmResource.clients();
        clientsResource.create(clientRepresentation);
    }


    @Operation(summary = "Create user",
            description = "string",
            tags = {"SWAGGER_TAG"})
    @PostMapping("/create")
    public ResponseEntity<String> createUser(
            @RequestParam String username,
            @RequestParam String email,
            @RequestParam String password) {

        RealmResource realmResource = keycloak.realm("todoapp-realm");
        UsersResource usersResource = realmResource.users();

        UserRepresentation user = new UserRepresentation();
        user.setUsername(username);
        user.setEmail(email);
        user.setEnabled(true);

        CredentialRepresentation credential = new CredentialRepresentation();
        credential.setType(CredentialRepresentation.PASSWORD);
        credential.setValue(password);

        user.setCredentials(Arrays.asList(credential));

        Response response = usersResource.create(user);
        if (response.getStatus() == 201) {
            String userId = CreatedResponseUtil.getCreatedId(response);
            return ResponseEntity.ok("Пользователь успешно создан. ID: " + userId);
        } else {
            return ResponseEntity.ok("Не удалось создать пользователя. Код ошибки: " + response.getStatus());
        }
    }
}
