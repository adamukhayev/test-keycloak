package com.example.demo.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/log")
@RequiredArgsConstructor
public class LoginController {

    @Operation(summary = "Get string",
            description = "string",
            tags = {"SWAGGER_TAG"})
    @GetMapping
    public String getString() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.isAuthenticated()) {
            String username = authentication.getName();
            System.out.println(authentication.getAuthorities());
            return "HELLO WORLD, User: " + username;
        } else {
            return "HELLO WORLD";
        }
    }

    @Operation(summary = "Create string",
            description = "string",
            tags = {"SWAGGER_TAG"})
    @PostMapping("/create")
    public String createString() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.isAuthenticated()) {
            String username = authentication.getName();
            System.out.println(authentication.getAuthorities());
            return "HELLO WORLD, User: " + username;
        } else {
            return "HELLO WORLD";
        }
    }
}
